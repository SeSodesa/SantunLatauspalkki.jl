# SantunLatauspalkki

A Julia package that defines a progress bar, which supports multi-threading.
**Note:** if the output is to a terminal, the terminal needs to support basic
ANSI escape codes, at least for now.

## Usage

To use the `Progressbar`, simply create one and then increment it in a loop of
your choosing:

	julia> using SantunLatauspalkki

	julia> prog_bar = SantunLatauspalkki.Progressbar("Tehtävä", 100003);

	julia> Threads.@threads for ii in 1 : 100003
		   SantunLatauspalkki.increment(prog_bar)
		   sleep( rand() / 100 )
		   end

One can also opt for a logging file instead of printing to `stdout`, in case
one wishes to leave a disowned computation running on a server:

	julia> using SantunLatauspalkki

	julia> prog_bar = SantunLatauspalkki.Progressbar("Tehtävä", 100003, "path/to/file.log");

	julia> Threads.@threads for ii in 1 : 100003
		   SantunLatauspalkki.increment(prog_bar)
		   sleep( rand() / 100 )
		   end

Notice that multi-threaded operation is supported, as the `increment` function
locks the contained `Channel` that is used to increment the waitbar.

## License

See the accompanying [LICENSE](./LICENSE) file.
