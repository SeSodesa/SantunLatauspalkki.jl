### Progressbar.jl
#
# Contains the type Progressbar and the functions defined on it.
#

"""
	Progressbar

A single progress bar.

"""
mutable struct Progressbar

	"""
		title

	The title of this progress bar item.

	"""
	const title :: String

	"""
		state

	Current state of this progress bar. Needs to be less than the maximum.

	"""
	state :: Int

	"""
		maximum

	The maximum value of this progress bar.

	"""
	const maximum :: Int

	"""
		print_interval

	Defines the number of increments of state before the progress bar is next
	updated on the designated display.

	"""
	const print_interval :: Int

	"""
		output_stream

	The output stream that this progress bar will be printed into.

	"""
	output_stream :: IO

	"""
		logging_file_path

	The file at the end of this path will rcontain the output of the progress
	bar. Initialized by calling the constructor with a String as the third
	argument.

	"""
	logging_file_path :: Union{String, Nothing}

	"""
		increment_queue

	The increment function should push a boolean into this channel when
	incrementing this `Progressbar`.

	"""
	increment_queue :: Channel{Bool}

	"""
		ProgressBar(title, maximum, output_stream::IO=stdout)

	An inner constructor for this type.

	"""
	function Progressbar(title::String, maximum::Int, output_stream :: IO = stdout)

		@assert ( 0 < maximum ) "The maximum value of a Progressbar needs to be positive."

		state = 0

		print_interval = ceil( maximum / 100 )

		queue_size = max( div(maximum, 2), 1)

		increment_queue = Channel{Bool}(queue_size)

		out_path = nothing

		bar = new( title, state, maximum, print_interval, output_stream, out_path, increment_queue )

		print(output_stream, bar)

		bar

	end # function

	"""
		ProgressBar(title, maximum, output_stream::AbstractString)

	An inner constructor for this type. Opens an IOStream of a given name,
	which will be used to output the progress bar during incrementation.

	"""
	function Progressbar(title::String, maximum::Int, output_stream::AbstractString)

		@assert ( 0 < maximum ) "The maximum value of a Progressbar needs to be positive."

		state = 0

		print_interval = ceil( maximum / 100 )

		queue_size = max( div( maximum, 2), 1 )

		increment_queue = Channel{Bool}(queue_size)

		out_path = abspath( output_stream )

		open(out_path, "w") do out_stream

			bar = new( title, state, maximum, print_interval, out_stream, out_path, increment_queue )

			print(out_stream, bar)

			bar

		end # do

	end # function

end # Progressbar

"""
	increment(bar)

Increments the state of a given `Progressbar` by pushing a Bool into its
increment queue and the immediately taking a value out of it.

"""
function increment(bar::Progressbar)

	put!( bar.increment_queue, true )

	if take!( bar.increment_queue )

		lock(bar.increment_queue)

		__increment(bar)

		unlock(bar.increment_queue)

	end # if

end # function

"""
	__increment(bar)

Does the actual incrementing of the waitbar.

"""
function __increment(bar::Progressbar)

	@assert ( bar.state < bar.maximum ) """
	For increment to work, the inner state of the progress bar needs to be \
	less than the maximum possible value.
	"""

	bar.state += 1

	needs_printing = mod(bar.state, bar.print_interval) == 0 || bar.state == bar.maximum

	if needs_printing

		print(bar.output_stream, bar)

	end

	if bar.state == bar.maximum

		println(bar.output_stream)

	end

	bar

end # function

"""
	Base.show(io, bar)

Prints the textual representation of a given `Progressbar`.

"""
function Base.show(io::IO, bar::Progressbar)

	print( io, ANSI_CLEAR_LINE, '\r' )

	show_str = appearance_of_( bar )

	print( io, show_str )

end # function

"""
	Base.show(io, bar)

Prints the textual representation of a given `Progressbar` into a text file.

"""
function Base.show(io::Base.IOStream, bar::Progressbar)

	show_str = appearance_of_( bar )

	rm(bar.logging_file_path)

	open(bar.logging_file_path, "w") do io

		print(io, show_str)

	end # do

end # function

"""
	appearance(bar)

Returns a string representation of a Progressbar.

"""
function appearance_of_(bar::Progressbar)

	percentage = Int( floor( 100 * bar.state / bar.maximum ) )

	precentage_complement = 100 - percentage

	return (
		bar.title
		*
		": "
		*
		string( bar.state )
		*
		" / "
		*
		string( bar.maximum )
		*
		" ("
		*
		string( percentage )
		*
		" %)"
	)

end # function
