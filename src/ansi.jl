### ansi.jl
#
# This file contains ANSI control sequences as constant string definitions and
# functions. There are also a few helper functions for making sure the
# functions get correct input.
#

"""
	ANSI_ESC

This escape character is used in constructing the ANSI Control Sequence
Initializer that prefixes many ANSI control sequences.

"""
const ANSI_ESC :: String = "\x1B"

"""
	ANSI_CSI

This Control Sequence Initializer is used to initialze ANSI control sequences.

"""
const ANSI_CSI :: String = ANSI_ESC * "["

"""
	ANSI_HOME

Moves the terminal cursor to position (0,0) when printed.

"""
const ANSI_HOME :: String = ANSI_CSI * "H"

"""
	ANSI_HIDE_CURSOR

Hides the cursor when printed.

"""
const ANSI_HIDE_CURSOR = ANSI_CSI * "?25l"

"""
	ANSI_SHOW_CURSOR

Shows the cursor when printed.

"""
const ANSI_SHOW_CURSOR = ANSI_CSI * "?25h"

"""
	ANSI_SAVE_CURSOR_POSITION

Saves the current cursor position when printed, so that it can be restored
later if needed.

"""
const ANSI_SAVE_CURSOR_POSITION = ANSI_CSI * "s"

"""
	ANSI_RESTORE_CURSOR_POSITION

Restores the saved cursor position when printed.

"""
const ANSI_RESTORE_CURSOR_POSITION = ANSI_CSI * "u"

"""
	ANSI_CLEAR_LINE

Clears the current line, when printed.

"""
const ANSI_CLEAR_LINE = ANSI_CSI * "2K"

"""
	ANSI_UP(number)

Moves a terminal cursor up by the given `number` of rows when printed into a
terminal.

"""
function ANSI_UP(number)

	ANSI_CSI * to_int_string(number) * "A"

end

"""
	ANSI_DOWN(number)

Moves a terminal cursor down by the given `number` of rows when printed into a
terminal.

"""
function ANSI_DOWN(number)

	ANSI_CSI * to_int_string(number) * "B"

end

"""
	ANSI_RIGHT(number)

Moves a terminal cursor right by the given `number` of columns when printed into a
terminal.

"""
function ANSI_RIGHT(number)

	ANSI_CSI * to_int_string(number) * "C"

end

"""
	ANSI_LEFT(number)

Moves a terminal cursor left by the given `number` of columns when printed into a
terminal.

"""
function ANSI_LEFT(number)

	ANSI_CSI * to_int_string(number) * "D"

end

#### Helper functions
#
# This section contains helper functions used by the above named ANSI
# sequences.
#

"""
	to_int_string(::Integer)

Converts an integer to a string, so that it might be appended to an ANSI
sequence.

"""
function to_int_string(number::T) where { T <: Integer }

	string(number)

end

"""
	to_int_string(::AbstractString)

Tries to parse a string as an integer, after which it is converted to a string
so that it might be appended to an ANSI control sequence.

"""
function to_int_string(number::T) where { T <: AbstractString }

	string(parse(Int, number))

end
